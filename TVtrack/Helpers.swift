//
//  Helpers.swift
//  TVtrack
//
//  Created by Jaroslav Hort on 24/04/2020.
//  Copyright © 2020 Jaroslav Hort. All rights reserved.
//

import Foundation
import NotificationBannerSwift

class Helpers {
    static func showErrorNotification(text: String) {
        let banner = GrowingNotificationBanner(title: "An error has occured", subtitle: text, style: .danger)
        banner.show()
    }
    
    static func showInfoNotification(text: String) {
        let banner = NotificationBanner(title: text, style: .info)
        banner.show()
    }
}
