//
//  TVMazeAPIDefinitions.swift
//  TVtrack
//
//  Created by Jaroslav Hort on 21/04/2020.
//  Copyright © 2020 Jaroslav Hort. All rights reserved.
//

import Foundation

public struct TVMazeShowSchedule: Codable, Hashable
{
    public let time: String
    public let days: [String]
}

public struct TVMazeShowRating: Codable, Hashable
{
    public let average: Float?
}

public struct TVMazeShowCountry: Codable, Hashable
{
    public let name: String
    public let code: String
    public let timezone: String
}

public struct TVMazeShowNetwork: Codable, Hashable
{
    public let id: Int
    public let name: String
    public let country: TVMazeShowCountry
}

public struct TVMazeShowWebChannel: Codable, Hashable
{
    public let id: Int
    public let name: String
    public let country: TVMazeShowCountry?
}

public struct TVMazeShowExternals: Codable, Hashable
{
    public let tvrage: Int?
    public let thetvdb: Int?
    public let imdb: String?
}

public struct TVMazeShowImage: Codable, Hashable
{
    public let medium: String
    public let original: String
}

public struct TVMazeShowLink: Codable, Hashable
{
    public let href: String
}

public struct TVMazeShowLinks: Codable, Hashable
{
    public let _self: TVMazeShowLink?
    public let previousepisode: TVMazeShowLink?
    
    enum CodingKeys: String, CodingKey {
        case _self = "self", previousepisode = "previousepisode"
    }
    
    public init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        _self = try values.decodeIfPresent(TVMazeShowLink.self, forKey: ._self) ?? nil
        previousepisode = try values.decodeIfPresent(TVMazeShowLink.self, forKey: .previousepisode) ?? nil
    }
}

public struct TVMazeShowEpisode: Codable, Hashable
{
    public let id: Int
    public let url: String
    public let name: String
    public let season: Int
    public let airdate: String
    public let airtime: String
    public let airstamp: String
    public let runtime: Int
    public let image: TVMazeShowImage?
    public let _links: TVMazeShowLinks
}

public struct TVMazeShowEmbedded: Codable, Hashable
{
    public let seasons: [TVMazeShowSeason]
    public let episodes: [TVMazeShowEpisode]
}

public struct TVMazeShowSeason: Codable, Hashable
{
    public let id: Int
    public let url: String
    public let number: Int?
    public let name: String
    public let premiereDate: String?
    public let endDate: String?
    public let network: TVMazeShowNetwork?
    public let webChannel: TVMazeShowWebChannel?
    public let image: TVMazeShowImage?
    public let summary: String?
    public let _links: TVMazeShowLinks
    public let _embedded: TVMazeShowEmbedded?
}

public struct TVMazeShow: Codable, Hashable
{
    public let id: Int
    public let url: String
    public let name: String
    public let type: String
    public let language: String?
    public let genres: [String]
    public let status: String?
    public let runtime: Int?
    public let premiered: String?
    public let officialSite: String?
    public let schedule: TVMazeShowSchedule
    public let rating: TVMazeShowRating
    public let weight: Int
    public let network: TVMazeShowNetwork?
    public let webChannel: TVMazeShowWebChannel?
    public let externals: TVMazeShowExternals
    public let image: TVMazeShowImage?
    public let summary: String?
    public let updated: Int
    public let _links: TVMazeShowLinks
    public let _embedded: TVMazeShowEmbedded?
}

public struct TVMazeShowSearch: Codable, Hashable
{
    public let score: Float
    public let show: TVMazeShow
}
