//
//  EpisodeCellTemplate.swift
//  TVtrack
//
//  Created by Jaroslav Hort on 22/04/2020.
//  Copyright © 2020 Jaroslav Hort. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift
import SkeletonView

class EpisodeCellTemplate: UITableViewCell
{
    @IBOutlet weak var episodeNameLabel: UILabel!
    @IBOutlet weak var episodeButton: UIButton!
    
    var apiEpisode: TVMazeShowEpisode!
    var dbEpisode: Episode?
    var apiShow: TVMazeShow!
    
    var realm : Realm!
    
    func setEpisode(apiEpisode: TVMazeShowEpisode, dbEpisode: Episode?, apiShow: TVMazeShow) {
        self.episodeNameLabel.text = apiEpisode.name
        self.apiEpisode = apiEpisode
        self.dbEpisode = dbEpisode
        self.apiShow = apiShow
        
        if(self.dbEpisode != nil && self.dbEpisode!.id == apiEpisode.id) {
            self.episodeButton!.setImage(UIImage(systemName: "checkmark.circle"), for: UIControl.State.normal)
        }
        else
        {
            self.episodeButton!.setImage(UIImage(systemName: "circle"), for: UIControl.State.normal)
        }
        
        self.episodeButton.snp.makeConstraints{ (main) in
            main.right.equalToSuperview().offset(-27)
            main.width.equalTo(30)
            main.top.equalToSuperview().offset(5)
        }
        
        self.episodeNameLabel.snp.makeConstraints { (main) in
            main.left.equalToSuperview().offset(40)
            main.right.equalTo(self.episodeButton.snp.left).offset(-20)
            main.top.equalToSuperview().offset(10)
        }
    }
    
    @IBAction func episodeButtonTouchUp(_ sender: Any) {
        if(dbEpisode == nil)
        {
            realm = try! Realm()
            
            let episode = Episode()
            episode.id = apiEpisode.id
            episode.name = apiEpisode.name
            episode.addedDate = Date()
            episode.imgurl = apiEpisode.image?.medium
            
            let showResult = realm.objects(Show.self).filter("id == \(apiShow.id)")
            
            if let show = showResult.first {
                episode.show = show
            }
            else
            {
                let show = Show()
                show.id = apiShow.id
                show.name = apiShow.name
                show.imgurl = apiShow.image!.medium
                show.addedDate = Date()
                
                try! realm.write {
                    realm.add(show)
                    Helpers.showInfoNotification(text: "Show Added")
                }
                
                episode.show = show
            }
            
            try! realm.write {
                realm.add(episode)
            }
            self.episodeButton!.setImage(UIImage(systemName: "checkmark.circle"), for: UIControl.State.normal)
        }
        else
        {
            try! realm!.write {
                realm.delete(dbEpisode!)
            }
            self.episodeButton!.setImage(UIImage(systemName: "circle"), for: UIControl.State.normal)
        }
    }
}
