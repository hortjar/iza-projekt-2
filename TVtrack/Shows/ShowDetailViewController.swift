//
//  AddShowDetailViewController.swift
//  TVtrack
//
//  Created by Jaroslav Hort on 21/04/2020.
//  Copyright © 2020 Jaroslav Hort. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import RealmSwift
import SkeletonView
import NotificationBannerSwift
import SnapKit

enum DetailSource {
    case Search
    case Home
}

class ShowDetailViewController: UIViewController, SkeletonTableViewDataSource, SkeletonTableViewDelegate {
    var selectedShowId: Int!
    
    @IBOutlet weak var seasonsTableView: UITableView!
    @IBOutlet weak var showDetailName: UILabel!
    @IBOutlet weak var showDateLabel: UILabel!
    @IBOutlet weak var showImage: UIImageView!
    @IBOutlet weak var showRatingLabel: UILabel!
    @IBOutlet weak var showAirtimeLabel: UILabel!
    @IBOutlet weak var showStationLabel: UILabel!
    @IBOutlet weak var showSummaryTextView: UITextView!
    @IBOutlet weak var showStatusLabel: UILabel!
    @IBOutlet weak var addShowButton: UIButton!
    
    var seasonsAndEpisodes = [Any]()
    
    var dataSource: TVMazeShow!
    var seasonsDataSoruce = [TVMazeShowSeason]()
    var episodesDataSoruce = [TVMazeShowEpisode]()
    
    var realm: Realm?
    
    var displaySource: DetailSource = DetailSource.Home
    
    func disableButton() {
        self.addShowButton.isEnabled = false
        self.addShowButton.backgroundColor = UIColor.clear
        self.addShowButton.setTitleColor(.label, for: .disabled)
        
        if self.displaySource == DetailSource.Search {
            self.addShowButton.setTitle("Show Already Added", for: .disabled)
        }
        else {
            self.addShowButton.isHidden = true
            self.addShowButton.snp.updateConstraints{ (make) in
                make.height.equalTo(0)
                make.bottom.equalTo(self.view.snp.bottom).offset(1)
            }
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        view.showAnimatedSkeleton()
        
        let url = URL(string: "https://api.tvmaze.com/shows/\(selectedShowId!)?embed[]=episodes&embed[]=seasons")
        
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if let _data = data {
                do {
                    if let stringData = String(data: _data, encoding: .utf8) {
                        let jsonResponse = try JSONDecoder().decode(TVMazeShow.self, from: stringData.data(using: .utf8)!)
                        
                        self.dataSource = jsonResponse
                        self.seasonsDataSoruce = jsonResponse._embedded!.seasons
                        self.episodesDataSoruce = jsonResponse._embedded!.episodes
                                                
                        for season in self.seasonsDataSoruce {
                            self.seasonsAndEpisodes.append(season)
                            for episode in self.episodesDataSoruce {
                                if episode.season == season.number {
                                    self.seasonsAndEpisodes.append(episode)
                                }
                            }
                        }
                        
                        let rating = "Rating: \((self.dataSource.rating.average ?? -1) > 0 ? String(format: "%.1f", self.dataSource.rating.average!) : "Unrated")"
                        
                        let premiereDate = "Premiered: \(jsonResponse.premiered != nil ? jsonResponse.premiered![0..<4] : "Unknown")"
                        
                        var airtime = (jsonResponse.status ?? "") == "Ended" ? "Aired " : "Airs "
                        
                        for day in jsonResponse.schedule.days {
                            airtime += "\(day),"
                        }
                        airtime = airtime[0..<airtime.count]
                        airtime += " at \(jsonResponse.schedule.time)"
                        
                        let summary = (self.dataSource.summary ?? "").replace(target: "<p>", withString: "").replace(target: "</p>", withString: "").replace(target: "<b>", withString: "").replace(target: "</b>", withString: "").replace(target: "<i>", withString: "").replace(target: "</i>", withString: "")
                        
                        let status = "Status: \(jsonResponse.status ?? "Unknown")"
                        
                        var station = "On "
                        
                        if(jsonResponse.network != nil) {
                            station += jsonResponse.network!.name
                        } else if (jsonResponse.webChannel != nil) {
                            station += jsonResponse.webChannel!.name
                        } else {
                            station += " unknown network"
                        }
                        
                        let threadRealm = try! Realm()
                        
                        let dbShow = threadRealm.objects(Show.self).filter("id = \(jsonResponse.id)")
                        
                        var showExists = false
                        if let _ = dbShow.first {
                            showExists = true
                        }
                        
                        var setImage = false
                        var imgUrl: URL!
                        
                        if let _image = jsonResponse.image
                        {
                            let httpsUrl =  _image.medium.replace(target: "http", withString: "https")

                            imgUrl = URL(string: httpsUrl)
                            
                            setImage = true
                        }
                        
                        DispatchQueue.main.async {
                            self.view.hideSkeleton()
                            
                            if showExists {
                                self.disableButton()
                            }
                            if setImage {
                                self.showImage.kf.setImage(with: imgUrl)
                            }

                            self.showDetailName.text = self.dataSource.name
                            self.showRatingLabel.text = rating
                            self.showAirtimeLabel.text = airtime
                            self.showSummaryTextView.text = summary
                            self.showStatusLabel.text = status
                            self.showDateLabel.text = premiereDate
                            self.showStationLabel.text = station
                            self.seasonsTableView.reloadData()
                        }
                    }
                }
                catch {
                    Helpers.showErrorNotification(text: error.localizedDescription)
                }
            }
            else if let _error = error {
                Helpers.showErrorNotification(text: _error.localizedDescription)
            }
        }
        
        task.resume()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.seasonsDataSoruce.count + self.episodesDataSoruce.count
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func collectionSkeletonView(_ skeletonView: UITableView, cellIdentifierForRowAt indexPath: IndexPath) -> ReusableCellIdentifier {
        return "episodeCell"
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //if indexPath.row >= seasonsAndEpisodes.count { return }
        
        let seasonOrEpisode = seasonsAndEpisodes[indexPath.row]
        
        if seasonOrEpisode is TVMazeShowSeason {
            let seasonCell = tableView.dequeueReusableCell(withIdentifier: "seasonCell") as! SeasonCellTemplate
            
            var episodesInSeason = [TVMazeShowEpisode]()
            for episode in episodesDataSoruce {
                if episode.season == (seasonOrEpisode as!TVMazeShowSeason).number {
                    episodesInSeason.append(episode)
                }
            }

            seasonCell.setSeason(apiSeason: seasonOrEpisode as! TVMazeShowSeason, apiEpisodes: episodesInSeason, show: dataSource)
            
            return seasonCell
        }
        else {
            let epCell = tableView.dequeueReusableCell(withIdentifier: "episodeCell") as! EpisodeCellTemplate
            
            let episode = realm!.objects(Episode.self).filter("id = \((seasonOrEpisode as! TVMazeShowEpisode).id)")
            
            epCell.setEpisode(apiEpisode: seasonOrEpisode as! TVMazeShowEpisode, dbEpisode: episode.first, apiShow: dataSource)
            
            return epCell
        }
    }
    
    @IBAction func addShowButtonTouchUp(_ sender: Any) {
        let show = Show()
        show.id = dataSource.id
        show.name = dataSource.name
        show.imgurl = dataSource.image!.medium
        show.addedDate = Date()
        
        try! realm!.write {
            realm!.add(show)
            self.disableButton()
            Helpers.showInfoNotification(text: "Show Added")
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.showDetailName.snp.makeConstraints { (make) in
            make.top.equalTo(view.snp.top).offset(20)
            make.width.equalToSuperview().offset(-15)
            make.left.equalTo(view.snp.left).offset(10)
            make.height.equalTo(60)
        }
        
        self.showImage.snp.makeConstraints { (make) in
            make.top.equalTo(self.showDetailName.snp.bottom)
            make.bottom.equalTo(self.showSummaryTextView.snp.top).offset(-10)
            make.width.equalTo(139)
            make.height.equalTo(220)
            make.left.equalTo(15)
        }
        
        self.showSummaryTextView.snp.makeConstraints { (make) -> Void in
            make.left.equalToSuperview().offset(15)
            make.right.equalToSuperview().offset(-15)
            make.height.greaterThanOrEqualTo(20)
            make.height.lessThanOrEqualTo(100)
        }
        
        self.seasonsTableView.snp.makeConstraints { (make) in
            make.top.equalTo(self.showSummaryTextView.snp.bottom).offset(17)
            make.bottom.equalTo(self.addShowButton.snp.top).offset(-30)
            make.width.equalToSuperview()
        }
        
        self.showRatingLabel.snp.makeConstraints{ (make) in
            make.top.equalTo(self.showDetailName.snp.bottom).offset(22)
            make.left.equalTo(self.showImage.snp.right).offset(15)
            make.right.equalToSuperview()
        }
        
        self.showDateLabel.snp.makeConstraints{ (make) in
            make.top.equalTo(self.showRatingLabel.snp.bottom).offset(20)
            make.left.equalTo(self.showImage.snp.right).offset(15)
            make.right.equalToSuperview()
        }
        
        self.showAirtimeLabel.snp.makeConstraints{ (make) in
            make.top.equalTo(self.showDateLabel.snp.bottom).offset(20)
            make.left.equalTo(self.showImage.snp.right).offset(15)
            make.right.equalToSuperview()
        }
        
        self.showStationLabel.snp.makeConstraints{ (make) in
            make.top.equalTo(self.showAirtimeLabel.snp.bottom).offset(20)
            make.left.equalTo(self.showImage.snp.right).offset(15)
            make.right.equalToSuperview()
        }
        
        self.showStatusLabel.snp.makeConstraints{ (make) in
            make.top.equalTo(self.showStationLabel.snp.bottom).offset(20)
            make.left.equalTo(self.showImage.snp.right).offset(15)
            make.right.equalToSuperview()
        }
        
        self.addShowButton.snp.makeConstraints{ (make) in
//            make.top.equalTo(self.seasonsTableView.snp.bottom)
            make.bottom.equalToSuperview().offset(-25)
            make.height.equalTo(35)
            make.left.equalToSuperview().offset(20)
            make.right.equalToSuperview().offset(-20)
        }
        
        seasonsTableView.separatorColor = UIColor.clear
        
        do {
            realm = try Realm()
        }
        catch {
            Helpers.showErrorNotification(text: error.localizedDescription)
            return
        }
    }
}
