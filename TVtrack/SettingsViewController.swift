//
//  SettingsViewController.swift
//  TVtrack
//
//  Created by Jaroslav Hort on 23/04/2020.
//  Copyright © 2020 Jaroslav Hort. All rights reserved.
//

import Foundation
import UIKit
import RealmSwift

class SettingsViewController: UIViewController {
    @IBOutlet weak var deleteAllDataButton: UIButton!
    
    @IBAction func deleteAllDataTouchUp(_ sender: Any) {
        let alert = UIAlertController(title: "Notice", message: "Are you sure you want to delete all data?", preferredStyle: UIAlertController.Style.alert)

        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Delete All", style: UIAlertAction.Style.destructive, handler: { (action) in
            let realm = try! Realm()

            try! realm.write {
                realm.deleteAll()
            }
        }))

        self.present(alert, animated: true, completion: nil)
    }
}
