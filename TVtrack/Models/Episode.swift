//
//  Episode.swift
//  TVtrack
//
//  Created by Jaroslav Hort on 23/04/2020.
//  Copyright © 2020 Jaroslav Hort. All rights reserved.
//

import Foundation
import RealmSwift

class Episode: Object {
    @objc dynamic var id = 0
    @objc dynamic var name = ""
    @objc dynamic var imgurl: String?
    @objc dynamic var addedDate = Date()
    @objc dynamic var show: Show?
    
    override class func primaryKey() -> String? {
        return "id"
    }
}
