![](logo.png)

## A simple iOS app to keep track of TV shows

### Requirements
* Xcode 11
* iOS 13

### Features
* Find TV shows
* See show details and ratings
* Keep track of air dates
* Keep track of watched seasons and episodes

### Libraries
* [Realm](https://realm.io/)
* [Kingfisher](https://github.com/onevcat/Kingfisher)
* [SkeletonView](https://github.com/Juanpe/SkeletonView)
* [NotificationBanner](https://github.com/Daltron/NotificationBanner)

#### Known issues
* Episodes don't visually mark as watched when marking seasons